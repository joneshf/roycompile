# Roy Compile

Compile Roy to Javascript without leaving sublime.

## Inspiration

This is greatly inspired by [@surjikal][surjikal]'s [Coffee Compile][coffee-compile]

## Usage

Compile the entire file by not selecting any text (or all the text).
Compile a section by selecting just that section.
Keyboard shortcut `ctrl-shift-R`
Command Pallette integration `Roy Compile`
Context menu `right click`

## Install

### Package Control

Now available in [@wbond][wbond]'s [package control][package-control].  Just bring up the package control menu in sublime (default `ctrl-shift-p`), and enter `Package Control: Install Package`, search for `RoyCompile`.

### Manual

Clone this repository from your Sublime packages directory:

#### Linux

```
$ cd ~/.config/sublime-text-2/Packages
$ git clone https://github.com/joneshf/RoyCompile
```

#### Macosx (untested)

```
$ cd "~/Library/Application Support/Sublime Text 2/Packages"
$ git clone https://github.com/joneshf/RoyCompile
```

#### Windows (untested)

```
$ cd "%APPDATA%\Sublime Text 2"
$ git clone https://github.com/joneshf/RoyCompile
```

[surjikal]: https://github.com/surjikal
[coffee-compile]: https://github.com/surjikal/sublime-coffee-compile
[wbond]: https://github.com/wbond
[package-control]: https://github.com/wbond/package_control_channel
